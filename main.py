# import requests as req
#
# response = req.get("http://api.open-notify.org/iss-now.json")
# response.raise_for_status()
#
#
# data = response.json()
# print(data)
#
# latitude = data['iss_position']['latitude']
# longitude = data['iss_position']['longitude']
#
# print((latitude, longitude))

import requests as req
from credentials import *
from datetime import datetime as dt

current_time = dt.now()

parameters = {
    "lat": MY_LAT,
    "lng": MY_LONG,
    'formatted': 0
}

response = req.get("https://api.sunrise-sunset.org/json", params=parameters)
response.raise_for_status()


data = response.json()

sunrise = data['results']['sunrise'].split('T')[1].split(':')[0]
sunset = data['results']['sunset'].split('T')[1].split(':')[0]



print(f"Sunrise: {sunrise}\nSunset: {sunset}\nCurrent: {current_time.hour}")

# print(*[{x: data['results'][x]} for x in data['results']], sep='\n')
